<?php  

	class Archivo{//Creacion de la clasa Archivo con nomenclatura UPPER CAMEL CASE
		
        //creacion de atributos (NOTA: atributos y varibles utilizan la nomenclatura LOWER CAMEL CASE)
		private $nombre;
		private $token;
        private $permitidos= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
        //metodo constructor con argumentos
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->token= substr(str_shuffle($this->permitidos), 0, 4);
		}

		//metodp que permite mostrar el mensaje y token(simbolo)
		public function mostrar(){
			return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
		}

		//metodo destructor sin argumentos
		public function __destruct(){
			//destruye token
			$this->token='El token ha sido destruido: '.$this->token;
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase Archivo
	$token1= new Archivo($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>