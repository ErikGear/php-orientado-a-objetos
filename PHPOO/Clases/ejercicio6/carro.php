<?php

include_once('transporte.php');

	class Carro extends transporte{
		private $num_cilindrada;

		
		public function __construct($nom,$vel,$com,$cilindrada){
			parent::__construct($nom,$vel,$com);
			$this->num_cilindrada=$cilindrada;
		}

		
		public function imprimirDetallesCarro(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Cilindrada:</td>
						<td>'. $this->num_cilindrada.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';


?>