<?php

include_once('transporte.php');

	class Combi extends transporte{
		private $num_ruta;

		
		public function __construct($nom,$vel,$com,$ruta){
			parent::__construct($nom,$vel,$com);
			$this->num_ruta=$ruta;
		}

		
		public function imprimirDetallesBarco(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de ruta de la combi:</td>
						<td>'. $this->num_ruta.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';


 ?>