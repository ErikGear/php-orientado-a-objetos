<?php
include_once('transporte.php');

class Avion extends transporte{

    private $turbinas;

    public function __construct($nombre,$velocidad,$combustible,$turbina)
    {
        parent::__construct($nombre,$velocidad,$combustible);
        $this->turbinas= $turbina;

    }

    public function imprimirDetallesAvion()
    {
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de turbinas:</td>
                    <td>'. $this->turbinas.'</td>				
                </tr>';
        return $mensaje;
    }

    
}
?>