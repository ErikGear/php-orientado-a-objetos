<?php  
include_once('transporte.php');

	class Barco extends transporte{
		private $tipo_mastil;

		
		public function __construct($nom,$vel,$com,$mastil){
			parent::__construct($nom,$vel,$com);
			$this->tipo_mastil=$mastil;
		}

		
		public function imprimirDetallesBarco(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Tipo de mastil:</td>
						<td>'. $this->tipo_mastil.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';


?>